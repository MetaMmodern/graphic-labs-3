import logging

STREAM_HANDLER = logging.StreamHandler()
FILE_HANDLER = logging.FileHandler(filename='output.log', mode='w')

_format = logging.Formatter('%(asctime)s | %(name)s | %(funcName)s | %(levelname)s | %(message)s')

STREAM_HANDLER.setFormatter(_format)
FILE_HANDLER.setFormatter(_format)


def wrap_logger(logger_name: str, level: int, *handlers):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    for handler in handlers:
        handler.setLevel(level)
        logger.addHandler(handler)
