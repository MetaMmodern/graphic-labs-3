import typing
import math
from ..base.image import BaseReader, BaseWriter, ImageData
from pic.errors import BMPImageValidationError, BMPBitError
from pic.utils import perform_pixels



class BmpReader(BaseReader):
    def read(self, path: str) -> ImageData:
        _bytes = self._read_file(path)

        # Validation
        self._validate_magic(_bytes, path)
        # self._validate_32_bits(_bytes)

        ####
        bmp_size = int.from_bytes(_bytes[2:6], 'little')
        pixel_array_address = int.from_bytes(_bytes[10:14], 'little')
        ####
        size_of_header = int.from_bytes(_bytes[14:18], 'little')
        width = int.from_bytes(_bytes[18:22], 'little')
        height = int.from_bytes(_bytes[22:26], 'little')
        bits_per_pixel = int.from_bytes(_bytes[28:30], 'little')

        size_of_pixel_map = pixel_array_address + width * height * int(bits_per_pixel / 8)

        pixels = list(map(int, _bytes[pixel_array_address:size_of_pixel_map]))
        pixel_table = perform_pixels(self._remove_offset(pixels, width),
                                     width, height,
                                     bytes_per_pixel=int(bits_per_pixel / 8))
        if bits_per_pixel == 24:
            return ImageData(width, height, pixel_table[::-1])

        if bits_per_pixel == 32:
            return ImageData(width, height, list(map(self._abgr_to_rgb, pixel_table[::-1])))
        else:
            raise BMPBitError(bit_count=bits_per_pixel)

    def _remove_offset(self, raw_pixels: typing.List[int], width: int) -> typing.List[int]:
        pixel = width
        while pixel < len(raw_pixels):
            del raw_pixels[pixel:pixel + (self._get_line_size(width) - width)]
            pixel += width
        return raw_pixels

    def _read_file(self, path) -> typing.Any:
        with open(path, 'rb') as i_file:
            return i_file.read()

    @staticmethod
    def _bgr_to_rgb(row: typing.List[int]):
        byte = 0
        while byte + 2 < len(row):
            row[byte], row[byte + 2] = row[byte + 2], row[byte]
            byte += 3
        return row

    @staticmethod
    def _abgr_to_rgb(row: typing.List[int]):
        def pixel_rgba_to_rgb(r: int, g: int, b: int, alpha: float):
            if alpha == 0:
                return r, g, b

            r = (1 - alpha) * 255 + alpha * r
            g = (1 - alpha) * 255 + alpha * g
            b = (1 - alpha) * 255 + alpha * b

            return round(r), round(g), round(b)

        new_row = []
        byte = 0
        while byte + 2 < len(row):
            r, g, b = pixel_rgba_to_rgb(
                row[byte + 2],
                row[byte + 1],
                row[byte],
                row[byte + 3] / 100,
            )
            new_row.append(r)
            new_row.append(g)
            new_row.append(b)
            byte += 4
        return new_row

    @staticmethod
    def _get_line_size(width: int) -> int:
        while width % 4:
            width += 1
        return width

    @staticmethod
    def _validate_magic(read_bytes: bytes, path: str):
        if read_bytes[:2] != b'BM':
            raise BMPImageValidationError(path)


class BmpWriter(BaseWriter):
    def write(self, path: str, image: ImageData):
        with open(path, 'wb') as bmp:
            bmp.write(b'BM')

            size_bookmark = bmp.tell()  # The next four bytes hold the filesize as a 32-bit
            bmp.write(bytes(4))  # little-endian integer. Zero placeholder for now.

            bmp.write(bytes(2))  # Unused 16-bit integer - should be zero
            bmp.write(bytes(2))  # Unused 16-bit integer - should be zero

            pixel_offset_bookmark = bmp.tell()  # The next four bytes hold the integer offset
            bmp.write(bytes(4))  # to the pixel data. Zero placeholder for now.

            # Image header
            bmp.write(bytes((40, 0, 0, 0)))  # Image header size in bytes - 40 decimal
            bmp.write(image.width.to_bytes(4, 'little'))  # Image width in pixels
            bmp.write(image.height.to_bytes(4, 'little'))  # Image height in pixels
            bmp.write(bytes((1, 0)))  # Number of image planes
            bmp.write(bytes((24, 0)))  # Bits per pixel 32
            bmp.write(bytes(4))  # No compression
            bmp.write(bytes(4))  # Zero for uncompressed images
            bmp.write(bytes(4))  # Unused pixels per meter
            bmp.write(bytes(4))  # Unused pixels per meter
            bmp.write(bytes(4))  # Use whole color table
            bmp.write(bytes(4))  # All colors are important

            # Pixel data
            pixel_data_bookmark = bmp.tell()
            for row in image.pixel_array[::-1]:  # BMP files are bottom to top
                row_data = bytes(self._rgb_to_bgr(row))
                bmp.write(row_data)
                padding = b'\x00' * ((4 - (len(row) % 4)) % 4)  # Pad row to multiple of four bytes
                bmp.write(padding)

            # End of file
            eof_bookmark = bmp.tell()

            # Fill in file size placeholder
            bmp.seek(size_bookmark)
            bmp.write(eof_bookmark.to_bytes(4, 'little'))

            # Fill in pixel
            bmp.seek(pixel_offset_bookmark)
            bmp.write(pixel_data_bookmark.to_bytes(4, 'little'))
            return image

    @staticmethod
    def _rgb_to_bgr(row: typing.List[int]):
        byte = 0
        while byte + 2 < len(row):
            row[byte], row[byte + 2] = row[byte + 2], row[byte]
            byte += 3
        return row
