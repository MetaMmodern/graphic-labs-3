import typing
from dataclasses import dataclass

int_matrix_type = typing.List[typing.List[int]]
float_matrix_type = typing.List[typing.List[float]]


@dataclass
class RawYCbCr:
    Y: int_matrix_type
    Cb: int_matrix_type
    Cr: int_matrix_type


@dataclass
class RawRGB:
    R: int_matrix_type
    G: int_matrix_type
    B: int_matrix_type

    def __iter__(self):
        for i in range(len(self.R)):
            for j in range(len(self.R[i])):
                yield self.R[i][j], self.G[i][j], self.B[i][j]


@dataclass
class Header:
    identifier: str
    version: str
    units: str
    density: typing.List[int]
    thumbnail: typing.List[int]

    def __repr__(self):
        return 'HEADER:\n\t' + '\n\t'.join([f'{k.upper()}: {v}' for k, v in self.__dict__.items()])


@dataclass
class QTable:
    destination: str
    mtx: int_matrix_type

    def __repr__(self):
        return f'Quantization table:\n' \
               f'\tDESTINATION: {self.destination}\n' \
               '\tMATRIX:\n\t\t' + '\n\t\t'.join(map(str, self.mtx))


@dataclass
class SOF:
    precision: int
    height: int
    width: int
    components_count: int
    components_data: typing.List[typing.Dict[str, str]]

    def __repr__(self):
        return 'SOF:\n\t' + '\n\t'.join([f'{k.upper()}: {v}' for k, v in self.__dict__.items()])


@dataclass
class HTable:
    table_class: int
    hft_map: typing.Dict[str, str]

    def __repr__(self):
        return f'HUFFMAN TABLE:\n' \
               f'\tCLASS: {self.table_class}\n' \
               f'\tMAP:\n' \
               f'\t\t' + '\n\t\t'.join([f'{k}: {v}' for k, v in self.hft_map.items()])


@dataclass
class SOS:
    components: typing.List[typing.Dict[str, int]]
    spectral_sel: int
    suc_approx: int

    def __repr__(self):
        return 'SOS:\n' + \
               f'\tCOMPONENTS:\n' + \
               '\t\t' + '\n\t\t'.join([str(i) for i in self.components]) + '\n' + \
               f'\tSPECTRAL_SEL: {self.spectral_sel}\n' \
               f'\tSUC_APPROX: {self.suc_approx}'


@dataclass
class DecodedData:
    header: Header
    q_tables: typing.List[QTable]
    sof: SOF
    h_tables: typing.List[HTable]
    sos: SOS
    raw_image_data: typing.List[int]

    def __repr__(self):
        return str(self.header) + '\n' + \
               '\n'.join(map(str, self.q_tables)) + '\n' + \
               str(self.sof) + '\n' + \
               '\n'.join(map(str, self.h_tables)) + '\n' + \
               str(self.sos) + '\n' + 'RAW IMAGE DATA:\n\t[hidden]'
