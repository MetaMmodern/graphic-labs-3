class HuffmanTable:
    def __init__(self):
        self.root = []
        self.elements = []

    def _bits_from_length(self, root, element, pos):
        if isinstance(root, list):
            if pos == 0:
                if len(root) < 2:
                    root.append(element)
                    return True
                return False
            for i in [0, 1]:
                if len(root) == i:
                    root.append([])
                if self._bits_from_length(root[i], element, pos - 1):
                    return True
        return False

    def get_huffman_bits(self, lengths, elements):
        self.elements = elements
        ii = 0
        for i in range(len(lengths)):
            for j in range(lengths[i]):
                self._bits_from_length(self.root, elements[ii], i)
                ii += 1

    def get_map(self):
        return self._get_map(self.root)

    def _get_map(self, node, acc=''):
        if isinstance(node, int):
            return {acc: hex(node)}
        else:
            l = {}
            r = {}
            for i in range(len(node)):
                if i == 0:
                    l = self._get_map(node[i], acc + '0')
                elif i == 1:
                    r = self._get_map(node[i], acc + '1')
            return {**l, **r}
