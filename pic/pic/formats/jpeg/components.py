import math
import logging
import numpy as np
from scipy.fftpack import idct

from .dataclass import *
from .utils import zig_zag_index, split
from .errors import JPEGComputingError

log = logging.getLogger('jpeg')


class M:
    JFIF_BYTE_0 = 0x00
    JFIF_BYTE_FF = 0xFF  # All markers start with this as the MSB
    JFIF_SOF0 = 0xC0  # Start of Frame 0, Baseline DCT
    JFIF_DHT = 0xC4  # Start of Frame 4, Differential Sequential DCT
    JFIF_SOI = 0xD8  # Start of Image
    JFIF_EOI = 0xD9  # End of Image
    JFIF_SOS = 0xDA  # Start of Scan
    JFIF_DQT = 0xDB  # Define Quantization Table
    JFIF_APP0 = 0xE0  # Application Segment 0, JPEG-JFIF Image
    JFIF_COM = 0xFE  # Comment

    @staticmethod
    def equal(marker: int, hex_val: str) -> bool:
        return M.merge_hex(M.JFIF_BYTE_FF, marker) == int(hex_val, 16)

    @staticmethod
    def merge_hex(x: int, y: int):
        return int(hex(x)[2:] + hex(y)[2:], 16)

    @staticmethod
    def bytes_to_int(_bytes: bytes):
        return int(_bytes.hex(), 16)


# --- Minimum Coded Unit Class ---

class MCU:
    def __init__(self, comp_rle: int_matrix_type, q_tables: typing.List[QTable], mcu_count='k'):
        self.comp_rle = comp_rle
        self.q_tables = q_tables
        self.mcu_count = mcu_count

    def convert_ycbcr_to_rgb(self) -> RawRGB:
        log.debug(f'Converting MCU #{self.mcu_count} to rgb')

        log.debug(f'Quantize MCU #{self.mcu_count}...')
        q_y = self._quantize_component(self.comp_rle[0], self.q_tables[0])
        q_cb = self._quantize_component(self.comp_rle[1], self.q_tables[1])
        q_cr = self._quantize_component(self.comp_rle[2], self.q_tables[1])

        log.debug(f'ZizZag MCU #{self.mcu_count}...')
        z_y = self._zigzag_component(q_y)
        z_cb = self._zigzag_component(q_cb)
        z_cr = self._zigzag_component(q_cr)

        log.debug('Undo DCT transform...')
        raw = RawYCbCr(
            Y=self._prettify(self._idct2(z_y)),
            Cb=self._prettify(self._idct2(z_cb)),
            Cr=self._prettify(self._idct2(z_cr))
        )

        log.debug('Converting YCbCr to RGB profile...')
        for i in range(8):
            for j in range(8):
                Y = raw.Y[i][j]
                Cb = raw.Cb[i][j]
                Cr = raw.Cr[i][j]

                r = round(Y + 1.402 * (Cr - 128))
                g = round(Y - 0.344136 * (Cb - 128) - 0.714136 * (Cr - 128))
                b = round(Y + 1.772 * (Cb - 128))

                raw.Y[i][j] = max(min(r, 255), 0)
                raw.Cb[i][j] = max(min(g, 255), 0)
                raw.Cr[i][j] = max(min(b, 255), 0)
        log.debug(f'Done converting MCU #{self.mcu_count} to RGB')
        return RawRGB(raw.Y, raw.Cb, raw.Cr)

    @staticmethod
    def _quantize_component(data: typing.List[int], q_table: QTable):
        for q_i in range(8):
            for q_j in range(8):
                data[q_i * 8 + q_j] *= q_table.mtx[q_i][q_j]
        return data

    @staticmethod
    def _zigzag_component(component: typing.List[int]) -> int_matrix_type:
        matrix_ij = int(math.sqrt(len(component)))
        matrix = [[0 for _ in range(matrix_ij)] for _ in range(matrix_ij)]

        for i in range(len(component)):
            m_i, m_j = zig_zag_index(i, matrix_ij)
            matrix[m_i][m_j] = component[i]
        return matrix

    @staticmethod
    def _limit_array(array, min_value=0, max_value=255):
        return list(map(lambda v: max(min(v, max_value), min_value), array))

    @staticmethod
    def _idct2(mtx: int_matrix_type) -> int_matrix_type:
        return idct(idct(mtx, axis=0, norm='ortho'), axis=1, norm='ortho')

    @staticmethod
    def _prettify(mtx):
        res = []
        for i in range(len(mtx)):
            subres = []
            for j in range(len(mtx[i])):
                subres.append(max(min(round(mtx[i][j]) + 128, 255), 0))
            res.append(subres)
        return res


# --- YCbCr Decoder ---
class YCbCrDecoder:
    def __init__(self, decoded_data: DecodedData,
                 bit_pos: typing.Optional[int] = 0):
        self.__decoded_data = decoded_data
        self.__bit_string: str = self._bytes_to_binary(self._remove_stuff_byte(decoded_data.raw_image_data))
        self.__bit_pos: int = bit_pos

    def decode(self) -> typing.List[int]:

        if self.__decoded_data.sof.components_count == 1:
            log.info('Detected black and white image. Processing...')
            mcu_array = self._compute_mcu_gray_image()
        elif self.__decoded_data.sof.components_count == 3:
            log.info('Detected color image. Processing...')
            mcu_array = self._compute_mcu_color_image()
        else:
            raise JPEGComputingError('Unknown JPEG components count')
        return self._mcu_to_rgb_map(mcu_array)

    def _mcu_to_rgb_map(self, mcu_array):
        log.info('Filling image...')
        rgb_blocks = [self._block_to_rgb(mcu.convert_ycbcr_to_rgb()) for mcu in mcu_array]
        rgb_map = []
        for i in range(int(self.__decoded_data.sof.height / 8)):
            horizontal_block = []
            for j in range(int(self.__decoded_data.sof.width / 8)):
                if not horizontal_block:
                    horizontal_block = rgb_blocks.pop(0)
                else:
                    horizontal_block = self._merge_blocks(horizontal_block, rgb_blocks.pop(0))
            for row in horizontal_block:
                rgb_map.extend(row)
        return rgb_map

    def _compute_mcu_gray_image(self):
        mcu_count = int((self.__decoded_data.sof.width * self.__decoded_data.sof.height) / 64)
        mcu_array: typing.List[MCU] = []

        log.info(f"Computing MCU's:"
                 f" Y Factor: 11"
                 f" ;MCU Count: {int(mcu_count / 1)}"
                 f" ;Compute method: YCbCr")

        log_divider = 500
        for f in range(mcu_count):
            log.debug(f'Computing MCU #{f}')
            log.debug('Lum Y')
            y_dc = self._decode_block_huffman_dc(self.__decoded_data.h_tables[0])
            y_ac = self._decode_block_huffman_ac(self.__decoded_data.h_tables[1])

            empty_block = [0 for _ in range(64)]

            if mcu_array:
                log.debug('Fixing DC')
                y_dc += mcu_array[-1].comp_rle[0][0]

            empty_q_table = QTable('unknown', [[0 for _ in range(8)] for _ in range(8)])
            self.__decoded_data.q_tables.append(empty_q_table)
            mcu_array.append(MCU([[y_dc] + y_ac, empty_block, empty_block], self.__decoded_data.q_tables, f))

            if f // log_divider:
                log.info(f"{f} MCU's computed")
                log_divider += 500
        log.info(f"MCU's computed. MCU count {len(mcu_array)}")

        return mcu_array

    def _compute_mcu_color_image(self):
        y_factor = self.__decoded_data.sof.components_data[0].get('factor')
        y_count = int(y_factor[0]) * int(y_factor[1])

        mcu_count = int((self.__decoded_data.sof.width * self.__decoded_data.sof.height) / 64)

        mcu_array: typing.List[MCU] = []

        log.info(f"Computing MCU's:"
                 f" Y Factor: {y_factor}"
                 f" ;MCU Count: {int(mcu_count / y_count)}"
                 f" ;Compute method: {'Y' * y_count} + CbCr")

        log_divider = 500
        for f in range(int(mcu_count / y_count)):

            log.debug(f'Computing MCU #{f}')
            y_array = []
            for y_i in range(y_count):
                log.debug(f'-- Lum Y{y_i} --')

                y_dc = self._decode_block_huffman_dc(self.__decoded_data.h_tables[0])
                # Fix DC diff
                if y_array:
                    y_dc += y_array[y_i - 1][0]

                y_ac = self._decode_block_huffman_ac(self.__decoded_data.h_tables[1])
                y_array.append([y_dc] + y_ac)

            log.debug(f'-- Chr Cb --')
            cb_dc = self._decode_block_huffman_dc(self.__decoded_data.h_tables[2])
            cb_ac = self._decode_block_huffman_ac(self.__decoded_data.h_tables[3])

            log.debug(f'-- Chr Cr --')
            cr_dc = self._decode_block_huffman_dc(self.__decoded_data.h_tables[2])
            cr_ac = self._decode_block_huffman_ac(self.__decoded_data.h_tables[3])

            # Fix DC diff
            if mcu_array:
                log.debug('Fixing DC')
                y_array[0][0] += mcu_array[-1].comp_rle[0][0]
                cb_dc += mcu_array[-1].comp_rle[1][0]
                cr_dc += mcu_array[-1].comp_rle[2][0]
            if y_count > 1:
                cb_array = self._stretch_chrominance([cb_dc] + cb_ac, y_count)
                cr_array = self._stretch_chrominance([cr_dc] + cr_ac, y_count)

                for y, cb, cr in zip(y_array, cb_array, cr_array):
                    mcu_array.append(MCU([y, cb, cr], self.__decoded_data.q_tables, f))
            else:
                mcu_array.append(MCU([y_array[0], [cb_dc] + cb_ac, [cr_dc] + cr_ac], self.__decoded_data.q_tables, f))

            if f // log_divider:
                log.info(f"{f} MCU's computed")
                log_divider += 500
        log.info(f"MCU's computed. MCU count {len(mcu_array)}")
        return mcu_array

    def _decode_block_huffman_dc(self, huffman: HTable) -> int:
        i = 0
        while self.__bit_pos + i < len(self.__bit_string):

            dc_header = self.__bit_string[self.__bit_pos:self.__bit_pos + i]

            if dc_header in huffman.hft_map.keys():
                value = int(huffman.hft_map[dc_header], base=16)
                self.__bit_pos += i
                log.debug(f'DC header: {dc_header}')

                if value == 0:
                    log.debug(f'DC decoded: 0')
                    return 0

                value_block = self.__bit_string[self.__bit_pos: self.__bit_pos + value]
                log.debug(f'DC value_block: {value_block}')

                if value_block[0] == '1':
                    self.__bit_pos += value
                    res = int(value_block, 2)
                    log.debug(f'DC decoded: {res}')
                    return res
                elif value_block[0] == '0':
                    self.__bit_pos += value
                    res = int(value_block, 2) - 2 ** len(value_block) + 1
                    log.debug(f'DC decoded: {res}')
                    return res
            else:
                i += 1
        raise JPEGComputingError('DC: bit pointer got > len(bit_string)')

    def _decode_block_huffman_ac(self, huffman: HTable) -> typing.List[int]:
        i = 0
        values = []
        while self.__bit_pos + 1 < len(self.__bit_string):
            if len(values) == 63:
                log.debug('[LENGTH 63]')

                log.debug(f'AC decoded: {values}')
                return values

            ac_header = self.__bit_string[self.__bit_pos:self.__bit_pos + i]
            if ac_header in huffman.hft_map.keys():
                log.debug(f'{self.__bit_pos} {len(self.__bit_string)}')
                self.__bit_pos += i

                log.debug('/\\/\\/\\')
                log.debug(f'AC header: {ac_header}')
                hex_value = huffman.hft_map[ac_header]

                hex_value = hex_value[2:]
                if hex_value == 'f0':
                    log.debug('Got ZRL. Adding 16 zeros')
                    values.extend([0 for _ in range(16)])
                    i = 0
                    continue
                if len(hex_value) > 1:
                    for _ in range(int(hex_value[0], base=16)):
                        values.append(0)
                    hex_value = hex_value[1]

                value = int(hex_value, base=16)

                if value == M.JFIF_BYTE_0:  # End of block
                    values.extend([0 for _ in range(63 - len(values))])  # Fill zeros
                    log.debug('[EOB]')
                    log.debug(f'AC decoded: {values}')
                    return values

                value_block = self.__bit_string[self.__bit_pos: self.__bit_pos + value]
                log.debug(f'AC value block: {value_block}')

                if value_block[0] == '1':
                    self.__bit_pos += value
                    res = int(value_block, 2)
                    log.debug(f'AC value: {res}')
                    log.debug("\\/\\/\\/")
                    values.append(res)
                    i = 0

                elif value_block[0] == '0':
                    self.__bit_pos += value
                    res = int(value_block, 2) - 2 ** len(value_block) + 1
                    log.debug(f'AC value: {res}')
                    log.debug("\\/\\/\\//")
                    values.append(res)
                    i = 0

                else:
                    raise JPEGComputingError('Block first value got not 0 or 1')

            else:
                i += 1
        log.debug(len(values))
        log.debug(self.__bit_pos)
        raise JPEGComputingError('AC: bit pointer got > len(bit_string)')

    @staticmethod
    def _stretch_chrominance(chrom: typing.List[int], coef: int):
        chrom = np.array(chrom)
        coef *= int(math.sqrt(coef))
        stretched_matrix = np.kron(chrom.reshape((coef, coef)), [[1, 1], [1, 1]])
        pieces = split(stretched_matrix, 8, 8)
        return list(map(lambda piece: piece.reshape((64, )).tolist(), pieces))

    @staticmethod
    def _merge_blocks(x: int_matrix_type, y: int_matrix_type) -> int_matrix_type:
        for x_i, y_i in zip(x, y):
            x_i.extend(y_i)
        return x

    @staticmethod
    def _block_to_rgb(rgb_abstraction: RawRGB) -> int_matrix_type:
        matrix = []

        for i in range(8):
            sub = []
            for r, g, b in zip(rgb_abstraction.R[i], rgb_abstraction.G[i], rgb_abstraction.B[i]):
                sub.append(r)
                sub.append(g)
                sub.append(b)
            matrix.append(sub)
        return matrix

    @staticmethod
    def _remove_stuff_byte(image: typing.List[int]):
        log.info('Removing 0xFF 0x00 byte...')
        i = 0
        for i, byte in enumerate(image):
            if byte == 255 and image[i + 1] == 0:
                del image[i + 1]
        log.info(f'Removed {i} bytes')
        return image

    @staticmethod
    def _bytes_to_binary(_bytes: typing.Union[bytes, typing.List[int]]):
        log.info('Converting bytes to bit string...')
        return ''.join(['{0:0>8b}'.format(value) for value in _bytes])

    @property
    def decoded_data(self):
        return self.__decoded_data

    @property
    def bit_position(self):
        return self.__bit_pos

    @property
    def bit_string(self):
        return self.__bit_string
