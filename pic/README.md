# PyImageConverter (PIC)

This project is an implementation of encoding/decoding different formats on **Python**

### Compatibility

Python version: `python >= 3.8`

Formats supported ⚙️:

* Readable: `bmp, ppm(P3/P6)`
* Writable: `bmp, ppm(P3)`

### Installing

* `pip intall -r requirements.txt`

### Usage

The usage is very simple! Just import `ImageFactory` from `pic.formats.factory`
and type your desired reader or writer.

Examples in `pic/examples`

### Docs:

#### PPM/PGM/PBM:

* http://paulbourke.net/dataformats/ppm/
* https://people.sc.fsu.edu/~jburkardt/data/ppma/ppma.html

#### BMP/BITMAP:

* https://en.wikipedia.org/wiki/BMP_file_format